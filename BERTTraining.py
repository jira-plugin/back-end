# -*- coding: utf-8 -*-
"""
Created on Fri Sep 18 20:02:07 2020

@author: Pavithran
"""



import sqlite3 as sq
from gensim import corpora, models, similarities
import requests
from requests.auth import HTTPBasicAuth
import scipy
import math
import JiraIssues as issues
from datetime import datetime
import torch
from sentence_transformers import SentenceTransformer,  SentencesDataset, LoggingHandler, losses, models, util
from sentence_transformers.evaluation import EmbeddingSimilarityEvaluator
from sentence_transformers.readers import STSBenchmarkDataReader, InputExample
from torch.utils.data import DataLoader
#HTML Formatting code
#-----------------------------------#
import html2text as h2t
h = h2t.HTML2Text()
h.unicode_snob = True
#--------------------#

#Obtaining and Pre-Processing the training dataset.
#-----------------------------------------------------------#
conn = sq.connect('AIAN_database.sqlite')
cur = conn.cursor()

cur.execute('SELECT * FROM confluence')
confluence_data = cur.fetchall()
cur.execute('SELECT * FROM google')
google_data = cur.fetchall()
documents = []
links = []
title = []
count = 0
while count < len(confluence_data):
    title.append(confluence_data[count][0])
    documents.append(h.handle(confluence_data[count][1]))
    links.append(confluence_data[count][2])
        
    count += 1

count = 0
while count < len(google_data): 
    title.append(google_data[count][0])
    documents.append(h.handle(google_data[count][1]))
    count+=1
    
cur.execute('SELECT * FROM training_dataset')
test_data = cur.fetchall()
labels = []
similar_documents  = []
for x in test_data:
    similar_documents.append(h.handle(x[1]))
    labels.append(int(float(x[2])))

training_data = []
for i in range(0,len(test_data)):
    a = (test_data[i][0], similar_documents[i],labels[i] )
    training_data.append(a)


conn.close()
#-----------------------------------------------------------#  

train_batch_size = 16
num_epochs = 4

dataset = training_data
model_name = 'bert-base-uncased'
word_embedding_model = models.Transformer(model_name)
pooling_model = models.Pooling(word_embedding_model.get_word_embedding_dimension(),
                               pooling_mode_mean_tokens=True,
                               pooling_mode_cls_token=False,
                               pooling_mode_max_tokens=False)
model = SentenceTransformer(modules=[word_embedding_model, pooling_model])

train_samples = []
dev_samples = []
test_samples = []
model_save_path = 'output/training_sim_function1_'+model_name.replace("/", "-")+'-'+datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
for data in dataset:
    input_example = InputExample(texts = [data[0],data[1]], label = data[2])
    test_samples.append(input_example)
    train_samples.append(input_example)
    dev_samples.append(input_example)
    
test_samples[:125]
train_samples[:1125]

train_dataset = SentencesDataset(train_samples, model)
train_dataloader = DataLoader(train_dataset, shuffle=True, batch_size=train_batch_size)
train_loss = losses.CosineSimilarityLoss(model=model)

evaluator = EmbeddingSimilarityEvaluator.from_input_examples(dev_samples, name='sts-dev')
warmup_steps = math.ceil(len(train_dataset) * num_epochs / train_batch_size * 0.1) #10% of train data for warm-up
model.fit(train_objectives=[(train_dataloader, train_loss)],
          evaluator=evaluator,
          epochs=num_epochs,
          evaluation_steps=1000,
          warmup_steps=warmup_steps,
          output_path=model_save_path)
