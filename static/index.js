//configuring Firebase
var firebaseConfig = {
    apiKey: "AIzaSyAWb-wsfXyDkbnDhLePG_Fny5aMt4HVqZc",
    authDomain: "jira-plugin-992bd.firebaseapp.com",
    databaseURL: "https://jira-plugin-992bd.firebaseio.com",
    projectId: "jira-plugin-992bd",
    storageBucket: "jira-plugin-992bd.appspot.com",
    messagingSenderId: "1057372609588",
    appId: "1:1057372609588:web:3aecde9e30a3b9e2d4be61",
    measurementId: "G-P0WVE5QCCC"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);


//reference url collection
var urlRef = firebase.database().ref('discussionLinks');

//listen for form submit event
document.getElementById('discussionUrlForm').addEventListener('submit', getIssues);

//generate issues by sending url as a parameter
function getIssues(e) {
    e.preventDefault();

    // get form values i.e, url
    var url = document.getElementById('discussion_link').value;
    //console.log(url)
    saveUrl(url);
}

//post the url to firebase; for testing purpose
function saveUrl(url) {
    var newUrlRef = urlRef.push();
    newUrlRef.set({
        discussion_url: url
    });
}