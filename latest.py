# -*- coding: utf-8 -*-
"""
Created on Thu Aug  6 16:37:27 2020

@author: Pavithran
"""


from __future__ import print_function
from atlassian import Confluence
import re
from bs4 import BeautifulSoup
from urllib.parse import urlparse
import pickle
import os.path

con_url = "https://confluence.sakaiproject.org"
confluence = Confluence(con_url)


def search_latest():
    """
    Gets 5 confluence documents created within the last 4 weeks.

    Returns
    -------
    Json answer

    """
    cql = "created >= now('-15w') and type = 'page'order by created"
    answers = confluence.cql(cql)
    x = []
    for answer in answers.get('results'):
        print(answer)
        x.append(answer)
    
    x = x[-5:]
    latest_confluence = []
    for s in x:
        url = con_url + s['url']
        tup1 = (s['title'],url)
        latest_confluence.append(tup1)
    #latest_confluence = []
    #latest_confluence.append(('2018-03-07 UX Group Meeting Agenda and Notes','https://docs.google.com/document/d/1gqL9Acwirx_SCS9OZxU_7ipTC6euqpvR_7bNXO45Amw'))
    return latest_confluence
    #https://confluence.sakaiproject.org/display/BBB/Home

#a = search_latest()
'''
#
cql = "url = '/display/DOC/19.5+Fixes+by+tool' and type = 'page' order by created"
answers = confluence.cql(cql)
pageid = '122617926'
page_data = confluence.get_page_by_id(pageid,expand='body.storage')
content=page_data['body']['storage']['value']
import html2text as h2t
h = h2t.HTML2Text()
h.unicode_snob = True
content = h.handle(content)
'''