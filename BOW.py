# -*- coding: utf-8 -*-
"""
Created on Mon Jul 27 16:42:44 2020

@author: Pavithran
"""

from gensim import corpora, models, similarities
import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

#Obtain the Confluence 
import os
import sys
import nltk
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer

import sqlite3 as sq
conn = sq.connect('AIAN_database.sqlite')
cur = conn.cursor()
cur.execute('SELECT * FROM confluence')
confluence_data = cur.fetchall()
cur.execute('SELECT * FROM google')
google_data = cur.fetchall()


documents = []
links = []

count = 0
while count < len(confluence_data):
    documents.append(confluence_data[count][1])
    links.append(confluence_data[count][2])
    count += 1

count = 0
while count < len(google_data):
    documents.append(google_data[count][1])
    count+=1

    #Clean the data 
texts = [[word for word in document.lower().split()] for document in documents]
nltk.download('punkt')
texts_tokenized = [[word.lower() for word in word_tokenize(document)] for document in documents]
#print(texts_tokenized
nltk.download('stopwords')
english_stopwords = stopwords.words('english')
texts_filtered_stopwords = [[word for word in document if not word in english_stopwords] for document in texts_tokenized]
    #print(texts_filtered_stopwords)
english_punctuations = [',','.',':',';','?','!','(',')','[',']','@','&','#','%','$','{','}','--','-','’','“'                           ,'”']
texts_filtered = [[word for word in document if not word in english_punctuations] for document in texts_filtered_stopwords]
    
   
st = PorterStemmer()   
texts_stemmed = [[st.stem(word) for word in document] for document in texts_filtered]
all_stems = sum(texts_stemmed,[])
stems_once = set(stem for stem in set(all_stems) if all_stems.count(stem) == 1)
texts_stemmed = [[stem for stem in text if stem not in stems_once] for text in texts_stemmed]  


    #Bag-Of-Words Dictionary
dictionary = corpora.Dictionary(texts_stemmed)
dictionary.save("AIAN_dict.dict")
    #Creating the Corpus
corpus = [dictionary.doc2bow(text, allow_update=True) for text in texts_stemmed]
corpora.MmCorpus.serialize('AIAN_corpus.mm', corpus)
    #print(dictionary.token2id)
#print(corpus)
'''
tfidf = models.TfidfModel(corpus)
corpus_tfidf = tfidf[corpus]
lsi_model = models.LsiModel(corpus_tfidf, id2word=dictionary, num_topics=10)
corpus_lsi = lsi_model[corpus_tfidf]
    
index = similarities.MatrixSimilarity(lsi_model[corpus])

en_str = description
en_str_vec = dictionary.doc2bow(en_str.lower().split())
    #print(en_str)
    #print(en_str_vec)

lsi_str_vec1 = lsi_model[en_str_vec]
sims = index[lsi_str_vec1]
simsorted = sorted(enumerate(sims), key=lambda item: -item[1])
'''