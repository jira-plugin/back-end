# -*- coding: utf-8 -*-
"""
Created on Mon Jun  1 21:53:10 2020

@author: Pavithran
"""

#Defining classes and functions for Binary Search Tree
#-----------------------------------#
class Node(object):
 
    def __init__(self, value):
        self.value = value
        self.left = None
        self.right = None
        self.count = 1

    def __str__(self):
        return 'value: {0}, count: {1}'.format(self.value, self.count)


def insert(root, value):
    if not root:
        return Node(value)
    elif root.value == value:
        root.count += 1
    elif value < root.value:
        root.left = insert(root.left, value)
    else:
        root.right = insert(root.right, value)

    return root

def create(seq):
    root = None
    for word in seq:
        root = insert(root, word)

    return root

def tupsearch(root, word, depth=1):
    if not root:
        return 0, 0
    elif root.value[0] == word:
        return depth, root.value
    elif word < root.value[0]:
        return tupsearch(root.left, word, depth + 1)
    else:
        return tupsearch(root.right, word, depth + 1)

def print_tree(root):
    if root:
        print_tree(root.left)
        print (root)
        print_tree(root.right)
#-----------------------------------#

try: 
    #Imported Files
    #-----------------------------------#
    from atlassian import Confluence
    import re
    from bs4 import BeautifulSoup
    from urllib.parse import urlparse
    import pickle
    import os.path
    from googleapiclient.discovery import build
    from google_auth_oauthlib.flow import InstalledAppFlow
    from google.auth.transport.requests import Request
    from googleapiclient.errors import HttpError as GoogleHttpError
    from flask import Flask, render_template, url_for, jsonify,redirect,send_from_directory,request,Blueprint, request
    from flask_ngrok import run_with_ngrok
    from flask_atlassian_connect import AtlassianConnect
    import os, json
    #--------------#
    
    #Loading the model
    #-----------------------------------#
    import es_core_news_sm
    nlp = es_core_news_sm.load()
    #---------------------------#

    #Internal Files
    #-----------------------------------#
    import JiraIssues as issues
    import JiraIssueGeneration as jiragen
    import latest
    import Function2 as f2
    import Similarities2 as f1
    import Summarizer
    #----------------#
    
    #from bs4 import BeautifulSoup
    
    #HTML Formatting code
    #-----------------------------------#
    import html2text as h2t
    h = h2t.HTML2Text()
    h.unicode_snob = True
    #--------------------#
    
    
    #General Summarizing modules.
    #-----------------------------------#
    from sumy.parsers.plaintext import PlaintextParser #We're choosing a plaintext parser here, other parsers available for HTML etc.
    from sumy.nlp.tokenizers import Tokenizer 
    #from sumy.summarizers.lsa import LsaSummarizer
    #from sumy.summarizers.text_rank import TextRankSummarizer #We're choosing Luhn, other algorithms are also built in
    from sumy.summarizers.lex_rank import LexRankSummarizer #We're choosing Lexrank, other algorithms are also built in
    #from sumy.summarizers.luhn import LuhnSummarizer #We're choosing Luhn, other algorithms are also built in
    #-----------------------------------#
    
    count = 0
    URL = "https://confluence.sakaiproject.org"
    googlebaseurl = 'https://docs.google.com/document/d/'
    confluence = Confluence(URL)
    app = Flask(__name__, static_url_path='')
    ac = AtlassianConnect(app)
    app.config['JSONIFY_PRETTYPRINT_REGULAR'] = False
    SITE_ROOT = os.path.realpath(os.path.dirname(__file__))
    filename = os.path.join(SITE_ROOT, "atlassian-connect.json")  
    summarizer_lsa = LexRankSummarizer()
    
   
    
    #Setting up SQLITE Database.
    #-----------------------------------#
    import sqlite3 as sq
    conn = sq.connect('AIAN_database.sqlite')
    cur = conn.cursor()
    #-----------------#
    
    #Importing Tables from the database.
    #-----------------------------------#
    cur.execute('SELECT * FROM confluence')
    confluence_data = cur.fetchall()
    cur.execute('SELECT * FROM google')
    google_data = cur.fetchall()
    #---------------------------#
    
    documents = []
    links = []
    title = []
    bstset = []
    count = 0
    while count < len(confluence_data):
        title.append(confluence_data[count][0])
        documents.append(confluence_data[count][1])
        links.append(confluence_data[count][2])
        s = (links[count], h.handle(confluence_data[count][1]))
        bstset.append(s)
        count += 1

    count = 0
    while count < len(google_data):
        title.append(google_data[count][0])
        documents.append(google_data[count][1])
        links.append(googlebaseurl + google_data[count][2])
        s = (googlebaseurl + google_data[count][2],google_data[count][1])
        bstset.append(s)
        count+=1
    bsttree = create(bstset)
    #url = 'https://confluence.sakaiproject.org/display/CONF10/Official+Slide+Template'
    #a = tupsearch(bsttree, url)
    #print(a)
    #http://127.0.0.1:5000/GRBK-6   
    
    #Flask Code
    #-----------------------------------#
    @app.route("/similarity/<string:issuekey>", methods=['POST', 'GET'])
    def hello(issuekey):
        finalresult = []
        description = issues.obtain_issues(issuekey)
        print(description)
        simresults = f1.Similairties(description)
        for i in range(0,5):
            result_index = simresults[i][0]
            result_title = title[result_index]
            result_link = links[result_index]
            result_content = documents[result_index]
            doc = nlp(result_content)
            result_summary = Summarizer.summarizer(doc)
            print(result_summary)
            finalresult.append((result_title,result_link,result_summary))
            
        return jsonify(finalresult)
        
    @app.route("/latest", methods = ['POST', 'GET'])
    def get_latest():
        latest_conf =  latest.search_latest()
        
        return jsonify(latest_conf)
   
    @app.route("/atconnect", methods = ['GET','POST'])
    def atconnect():
        return redirect(url_for('static', filename='atlassian-connect.json'))
    
    @app.route("/issuegen/<path:url>", methods=['POST','GET'])
    def generate_issues(url):
        print(url)
        bstresult = tupsearch(bsttree, url)
        print(bstresult)
        content = bstresult[1][1]
        parser = PlaintextParser.from_string(content,Tokenizer("english"))
        i_num = 10
        #i_num = f2.issue_num_tpm(content)
        summary_2 =summarizer_lsa(parser.document,i_num) #Summarize the document with 7 sentences
        for sentence in summary_2:
            print(sentence)
            print('\n\n')
        
        issue = f2.issue_generator(summary_2)
        return jsonify(issue)
    
    @app.route("/issuegen/submit", methods=['POST','GET'])
    def submissionhandler():
        print(request.is_json)
        if request.is_json:
            content = request.get_json()
            content.update({'project': {'id': 10000}})
            content.update({'issuetype': {'name': 'Bug'}})
            print(content)
            created_issue = jiragen.issuegen(content)
            issue_response = {"Key":str(created_issue)}
            return jsonify(issue_response)
        else:
            return 'Invalid data recieved. Please try again'
            
        
            
    if __name__ == '__main__':
        app.run(port = 5000)
    #-----------------------------------#

except TimeoutError:  
          print('We don\'t have access to this file. ')
          count += 1
 