
import sqlite3 as sq
#import Similarity_files as sims
conn = sq.connect('AIAN_database.sqlite')
cur = conn.cursor()

googlebaseurl = 'https://docs.google.com/document/d/'
cur.execute('SELECT * FROM confluence')
confluence_data = cur.fetchall()
cur.execute('SELECT * FROM google')
google_data = cur.fetchall()

documents = []
links = []
title = []
linktest = []
count = 0
while count < len(confluence_data):
    title.append(confluence_data[count][0])
    documents.append(confluence_data[count][1])
    links.append(confluence_data[count][2])
    s = (links[count], documents[count])
    linktest.append(s)
        
    count += 1

count = 0
while count < len(google_data): 
    title.append(google_data[count][0])
    documents.append(google_data[count][1])
    links.append(googlebaseurl + google_data[count][2])
    s = (googlebaseurl + google_data[count][2],google_data[count][1])
    linktest.append(s)
    count+=1
    
class Node(object):
 
    def __init__(self, value):
        self.value = value
        self.left = None
        self.right = None
        self.count = 1

    def __str__(self):
        return 'value: {0}, count: {1}'.format(self.value, self.count)


def insert(root, value):
    if not root:
        return Node(value)
    elif root.value == value:
        root.count += 1
    elif value < root.value:
        root.left = insert(root.left, value)
    else:
        root.right = insert(root.right, value)

    return root

def create(seq):
    root = None
    for word in seq:
        root = insert(root, word)

    return root

def tupsearch(root, word, depth=1):
    if not root:
        return 0, 0
    elif root.value[0] == word:
        return depth, root.value
    elif word < root.value[0]:
        return tupsearch(root.left, word, depth + 1)
    else:
        return tupsearch(root.right, word, depth + 1)

def print_tree(root):
    if root:
        print_tree(root.left)
        print (root)
        print_tree(root.right)



#linktree = create(linktest)
#print_tree(linktree)
#a = tupsearch(linktree, 'https://docs.google.com/document/d/1Nk8EpN9hlnIP68CuUl_V3WnQ5dhcj-moSmFKqX3t8Dk')
#print(a)
