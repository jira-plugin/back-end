# -*- coding: utf-8 -*-
"""
Created on Sat Jul 18 23:46:21 2020

@author: Pavithran
"""
import requests, pprint
from flask import Flask, render_template, url_for, jsonify,redirect,send_from_directory,request
from flask_ngrok import run_with_ngrok
from flask_atlassian_connect import AtlassianConnect
import os, json

app = Flask(__name__, static_url_path='')


app.config['JSONIFY_PRETTYPRINT_REGULAR'] = False
SITE_ROOT = os.path.realpath(os.path.dirname(__file__))
filename = os.path.join(SITE_ROOT, "atlassian-connect.json")  

@app.route("/", methods=['POST', 'GET'])
def hello():
    if request.method == 'POST':
        issue_key = request.form['issue_link']
        response = requests.get("http://127.0.0.1:5000/"+issue_key)
        pp = pprint.PrettyPrinter(indent=4)
        pp.pprint(response.json())
        return response.json()

      
if __name__ == '__main__':
      app.run(port=8080) 

      









"""
Created on Sat Jul 18 23:46:21 2020

@author: Pavithran


try:
    from flask import Flask, render_template, url_for, jsonify,redirect,send_from_directory,request
    from flask_ngrok import run_with_ngrok
    from flask_atlassian_connect import AtlassianConnect
    import os, json
    import JiraIssues as issues
    import JiraAPi as doc_obtainer
    import Similarity_files as sims
    from bs4 import BeautifulSoup
    import html2text
    count = 0
    #sim_list = sims.simsorted
    #docs = sims.documents
   # sim5 = []
    
    app = Flask(__name__, static_url_path='')
    ac = AtlassianConnect(app)
    app.config['JSONIFY_PRETTYPRINT_REGULAR'] = False
    SITE_ROOT = os.path.realpath(os.path.dirname(__file__))
    filename = os.path.join(SITE_ROOT, "atlassian-connect.json")  
   
    google_content = doc_obtainer.google_docs_content
    docs_with_links = doc_obtainer.Text_with_links
    google_links = doc_obtainer.google_docs
    doc_titles = doc_obtainer.Confluence_titles
    documents = []
    links = []
#for x,y in Confluence_content.items():
#    documents.append(y)
    

    for x,y in docs_with_links.items():
        documents.append(y)
        links.append(x)
    
    for x,y in google_content.items():
        documents.append(y)
    
    if google_links:
        links.append(google_links)
    
    @app.route("/", methods=['POST', 'GET'])
    def hello():
        if request.method == 'POST':
            issue_key = request.form['issue_link']
            #print(issue_key)
            description = issues.obtain_issues(issue_key)
            highest_percentage,best_sim,best_links,best_titles = sims.similarities_func(issue_key, documents)
            best_disc = best_sim[issue_key][0]
            best_link = best_links[issue_key][0]
            best_title = best_titles[issue_key][0]
            #print(html2text.html2text(best_disc))
            disc2,disc3,disc4,disc5 = best_sim[issue_key][1],best_sim[issue_key][2],best_sim[issue_key][3],best_sim[issue_key][4]
            link2,link3,link4,link5 = best_links[issue_key][1],best_links[issue_key][2],best_links[issue_key][3],best_links[issue_key][4]
            title2,title3,title4,title5 = best_titles[issue_key][1],best_titles[issue_key][2],best_titles[issue_key][3],best_titles[issue_key][4]
            return redirect(url_for('related', issue_key = issue_key,description = description,best_disc = html2text.html2text(best_disc)
                                    , disc2 =  html2text.html2text(disc2), disc3 =  html2text.html2text(disc3),
                                    disc4 =  html2text.html2text(disc4), disc5 =  html2text.html2text(disc5),
                                    best_link = best_link, link2 = link2, link3 = link3, link4 = link4,
                                    link5 = link5
                                    , best_title = best_title, title2 = title2,title3 = title3,title4 = title4
                                    ,title5 = title5 ))
        
        return render_template("helloworld.html")
    #http://127.0.0.1:5000/atconnect
    @app.route("/atconnect", methods = ['GET','POST'])
    def atconnect():
        return redirect(url_for('static', filename='atlassian-connect.json'))
        
    @app.route("/related")
    def related():
        description = request.args.get('description', None)
        issue_key = request.args.get('issue_key', None)
        best_disc = request.args.get('best_disc')
        disc2 = request.args.get('disc2')
        disc3 = request.args.get('disc3')
        disc4 = request.args.get('disc4')
        disc5 = request.args.get('disc5')
        best_link = request.args.get('best_link')
        link2 = request.args.get('link2')
        link3 = request.args.get('link3')
        link4 = request.args.get('link4')
        link5 = request.args.get('link5')
        best_title = request.args.get('best_title')
        title2 = request.args.get('title2')
        title3 = request.args.get('title3')
        title4 = request.args.get('title4')
        title5 = request.args.get('title5')
        return render_template("relatedConfluenceLinks.html",issue_key=issue_key, description=description, best_disc = html2text.html2text(best_disc)
                               ,disc2 =  html2text.html2text(disc2), disc3 =  html2text.html2text(disc3)
                               ,disc4 =  html2text.html2text(disc4), disc5 =  html2text.html2text(disc5)
                               ,best_link = best_link,link2 = link2,link3 = link3,link4 = link4,link5 = link5
                               ,best_title = best_title,title2 = title2,title3 = title3,title4 = title4,title5 = title5)

          
     
    if __name__ == '__main__':
        app.run()
    
except TimeoutError:
          print('We don\'t have access to this file. ')
          count += 1
"""


