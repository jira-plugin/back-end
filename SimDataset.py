# -*- coding: utf-8 -*-
"""
Created on Mon Aug  3 12:13:29 2020

@author: Pavithran
"""

import Similarity_files as sims
import sqlite3 as sq
conn = sq.connect('AIAN_database.sqlite')
cur = conn.cursor()

issuekey = 'SAK-43199'
cur.execute('SELECT * FROM issues')
issuedata = cur.fetchall()
cur.execute('SELECT * FROM confluence')
confluence_data = cur.fetchall()
cur.execute('SELECT * FROM google')
google_data = cur.fetchall()

sql_command = """
DROP TABLE IF EXISTS simdataset;
CREATE TABLE simdataset (
    title VARCHAR,
    content VARCHAR);
"""
cur.executescript(sql_command)
conn.commit()

smalldata = issuedata[0:1000]
documents = []
links = []
titles = []
count = 0
while count < len(confluence_data):
    titles.append(confluence_data[count][0])
    documents.append(confluence_data[count][1])
    links.append(confluence_data[count][2])
    count += 1

count = 0
while count < len(google_data):
    titles.append(google_data[count][0])
    documents.append(google_data[count][1])
    count+=1



best_sim = {}
simpercent = {} 
bestsim_link = {}
bestsim_title = {}
summar_sim = {}
main_count = 0
simsorted_separate = {}

count = 0
while count < len(smalldata):
    description = smalldata[count][1]
    simsorted = sims.obtain_similarities(description)
    dockey = simsorted[count][0]
    cur.execute('INSERT INTO simdataset (title,content) values (?,?)', (smalldata[count][0] ,documents[dockey] ))
    conn.commit()
    count+= 1

cur.execute('SELECT * FROM simdataset')
sim_data = cur.fetchall()

'''
'''