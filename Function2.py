# In[1]:

# Import necessary things
from attention import AttentionLayer
import numpy as np
import pandas as pd 
import re
from bs4 import BeautifulSoup
from keras.preprocessing.text import Tokenizer 
from keras.preprocessing.sequence import pad_sequences
from nltk.corpus import stopwords
from tensorflow.keras.layers import Input, LSTM, Embedding, Dense, Concatenate, TimeDistributed
from tensorflow.keras.models import Model
from tensorflow.keras.callbacks import EarlyStopping
import warnings
pd.set_option("display.max_colwidth",200)
warnings.filterwarnings("ignore")


# In[2]:
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.models import load_model
#encoder_model.save("/content/drive/My Drive/Colab Notebooks/E_M2.h5")
#decoder_model.save("/content/drive/My Drive/Colab Notebooks/D_M2.h5")
#model.save("/content/drive/My Drive/Colab Notebooks/trainedmodel_50Epoch2.h5")

encoder_model = load_model('E_M2.h5', custom_objects={'AttentionLayer': AttentionLayer})
decoder_model = load_model('D_M2.h5', custom_objects={'AttentionLayer': AttentionLayer})
model = load_model('trainedmodel_50Epoch2.h5', custom_objects={'AttentionLayer': AttentionLayer})
import pickle
# saving
#with open('/content/drive/My Drive/Colab Notebooks/x_tokenizer.pickle', 'wb') as handle:
#    pickle.dump(x_tokenizer, handle, protocol=pickle.HIGHEST_PROTOCOL)
#with open('/content/drive/My Drive/Colab Notebooks/y_tokenizer.pickle', 'wb') as handle:
#    pickle.dump(y_tokenizer, handle, protocol=pickle.HIGHEST_PROTOCOL)
# loading
with open('x_tokenizer.pickle', 'rb') as handle:
    x_tokenizer = pickle.load(handle)
with open('y_tokenizer.pickle', 'rb') as handle:
    y_tokenizer = pickle.load(handle)


# In[3]:


reverse_target_word_index=y_tokenizer.index_word
reverse_source_word_index=x_tokenizer.index_word
target_word_index=y_tokenizer.word_index


max_text_len=180
max_summary_len=25


# In[4]:


contraction_mapping = {"ain't": "is not", "aren't": "are not","can't": "cannot", "'cause": "because", "could've": "could have", "couldn't": "could not",
                           "didn't": "did not",  "doesn't": "does not", "don't": "do not", "hadn't": "had not", "hasn't": "has not", "haven't": "have not",
                           "he'd": "he would","he'll": "he will", "he's": "he is", "how'd": "how did", "how'd'y": "how do you", "how'll": "how will", "how's": "how is",
                           "I'd": "I would", "I'd've": "I would have", "I'll": "I will", "I'll've": "I will have","I'm": "I am", "I've": "I have", "i'd": "i would",
                           "i'd've": "i would have", "i'll": "i will",  "i'll've": "i will have","i'm": "i am", "i've": "i have", "isn't": "is not", "it'd": "it would",
                           "it'd've": "it would have", "it'll": "it will", "it'll've": "it will have","it's": "it is", "let's": "let us", "ma'am": "madam",
                           "mayn't": "may not", "might've": "might have","mightn't": "might not","mightn't've": "might not have", "must've": "must have",
                           "mustn't": "must not", "mustn't've": "must not have", "needn't": "need not", "needn't've": "need not have","o'clock": "of the clock",
                           "oughtn't": "ought not", "oughtn't've": "ought not have", "shan't": "shall not", "sha'n't": "shall not", "shan't've": "shall not have",
                           "she'd": "she would", "she'd've": "she would have", "she'll": "she will", "she'll've": "she will have", "she's": "she is",
                           "should've": "should have", "shouldn't": "should not", "shouldn't've": "should not have", "so've": "so have","so's": "so as",
                           "this's": "this is","that'd": "that would", "that'd've": "that would have", "that's": "that is", "there'd": "there would",
                           "there'd've": "there would have", "there's": "there is", "here's": "here is","they'd": "they would", "they'd've": "they would have",
                           "they'll": "they will", "they'll've": "they will have", "they're": "they are", "they've": "they have", "to've": "to have",
                           "wasn't": "was not", "we'd": "we would", "we'd've": "we would have", "we'll": "we will", "we'll've": "we will have", "we're": "we are",
                           "we've": "we have", "weren't": "were not", "what'll": "what will", "what'll've": "what will have", "what're": "what are",
                           "what's": "what is", "what've": "what have", "when's": "when is", "when've": "when have", "where'd": "where did", "where's": "where is",
                           "where've": "where have", "who'll": "who will", "who'll've": "who will have", "who's": "who is", "who've": "who have",
                           "why's": "why is", "why've": "why have", "will've": "will have", "won't": "will not", "won't've": "will not have",
                           "would've": "would have", "wouldn't": "would not", "wouldn't've": "would not have", "y'all": "you all",
                           "y'all'd": "you all would","y'all'd've": "you all would have","y'all're": "you all are","y'all've": "you all have",
                           "you'd": "you would", "you'd've": "you would have", "you'll": "you will", "you'll've": "you will have",
                           "you're": "you are", "you've": "you have"}


# In[5]:


import nltk
nltk.download('stopwords')
stop_words = set(stopwords.words('english')) 

def text_cleaner(text,num):
    newString = text.lower()
    newString = BeautifulSoup(newString, "lxml").text
    newString = re.sub(r'\([^)]*\)', '', newString)
    newString = re.sub('"','', newString)
    newString = ' '.join([contraction_mapping[t] if t in contraction_mapping else t for t in newString.split(" ")])    
    newString = re.sub(r"'s\b","",newString)
    newString = re.sub("[^a-zA-Z]", " ", newString) 
    newString = re.sub('[m]{2,}', 'mm', newString)
    if(num==0):
        tokens = [w for w in newString.split() if not w in stop_words]
    else:
        tokens=newString.split()
    long_words=[]
    for i in tokens:
        if len(i)>1:                                                 #removing short word
            long_words.append(i)   
    return (" ".join(long_words)).strip()


# In[12]:


def decode_sequence(input_seq):
    # Encode the input as state vectors.
    e_out, e_h, e_c = encoder_model.predict(input_seq)
    
    # Generate empty target sequence of length 1.
    target_seq = np.zeros((1,1))
    
    # Populate the first word of target sequence with the start word.
    target_seq[0, 0] = target_word_index['sostok']

    stop_condition = False
    decoded_sentence = ''
    while not stop_condition:
      
        output_tokens, h, c = decoder_model.predict([target_seq] + [e_out, e_h, e_c])

        # Sample a token
        sampled_token_index = np.argmax(output_tokens[0, -1, :])
        sampled_token = reverse_target_word_index[sampled_token_index]
        
        if(sampled_token!='eostok'):
            decoded_sentence += ' '+sampled_token

        # Exit condition: either hit max length or find stop word.
        if (sampled_token == 'eostok'  or len(decoded_sentence.split()) >= (max_summary_len-1)):
            stop_condition = True

        # Update the target sequence (of length 1).
        target_seq = np.zeros((1,1))
        target_seq[0, 0] = sampled_token_index

        # Update internal states
        e_h, e_c = h, c

    return decoded_sentence


# In[10]:


def title_create(i_descrip):
  t = i_descrip
  t = text_cleaner(t,0)
  t = x_tokenizer.texts_to_sequences([t])
  t = pad_sequences(t, maxlen=max_text_len, padding='post')
  title = decode_sequence(t[0].reshape(1,max_text_len))
  title=title.split(' ')
  q = []
  for i in range(0,len(title)):
    if(i+1!=len(title)):
      if(title[i]==title[i+1]):
        q.append(i)
  for i in q:
    del title[i]
  title = ' '.join(title)
  return title.strip().capitalize()

# An example to use this function

# In[13]:

'''
w = """OOTB gradebook:
Consider a site with sections/groups. If a member of a section has the "TA" marker, he/she is able to view and enter grades for all of the gradebook items for all of the students in his/her section.

There is a need to expand or restrict this permission structure to allow an instructor to assign these "TA"s permissions by category and section. We will be adding a new page in which the instructor can create grader "rules" that will override the default TA permissions in the gradebook.

The rules will have the following structure denoted by drop down menus:
Theresa TA can GRADE/VIEW......ANY SECTION/SPECIFIC SECTION......in......ANY CATEGORY/SPECIFIC CATEGORY

For instance, Theresa TA may be able to Grade everything in Section 3 for the Assignments Category and nothing else.

The instructor may add multiple rules for each user, and the permissions granted will be cumulative.

If there are no categories, the rules will not include the category restriction and vice versa for Sections.

A spec will be attached when it is completed."""
title_create(w)

'''
# Extractive method to get the summary

# 1. A key word function

# In[19]:


#!pip install pytextrank
import spacy
import pytextrank


# In[21]:

'''
# example text
text = "Add JIRAs or comments for skin stuff you like fixed for 20.0', 'An idea was for a user-based setting to either display \"icon only\" for the \"Link," "Help,\" and \"Full Screen\" buttons or icon + text label."
#text = str(summary_2[1])
# load a spaCy model, depending on language, scale, etc.
nlp = spacy.load("en_core_web_sm")

# add PyTextRank to the spaCy pipeline
tr = pytextrank.TextRank()
nlp.add_pipe(tr.PipelineComponent, name="textrank", last=True)

doc = nlp(text)

# examine the top-ranked phrases in the document
for s in doc._.phrases:
    print("{:.4f} {:5d}  {}".format(s.rank, s.count, s.text))
    print(s.chunks)

'''
# In[22]:


def key_word_func(t):
  text = str(t)
# load a spaCy model, depending on language, scale, etc.
  nlp = spacy.load("en_core_web_sm")
# add PyTextRank to the spaCy pipeline
  tr = pytextrank.TextRank()
  nlp.add_pipe(tr.PipelineComponent, name="textrank", last=True)
  doc = nlp(text)
# examine the top-ranked phrases in the document
  trphrases = []
  for s in doc._.phrases:
      trphrases.append(str(s.chunks))
    #print(s.chunks, end='')
  return trphrases

# A text for demostrating

# In[23]:


p = '''
A choice often available in other gradebook tools is whether the student grade calculation uses a running total or not.  In Sakai's Gradebook 1.0, there is no choice -- it's simply a Running Total.  While there is now an optional feature to display the overall total points (SAK-18588) that a student has earned out of the overall total points possible, the grade calculation is still a running total.  Several professors at our institution have requested that this be an option that they can choose, to avoid student confusion.
'''


# How to turn a text into sequences for each \n

# In[ ]:


#p = p.replace("\n","<stop>")
#sentences = p.split("<stop>")
#sentences = sentences[:-1]
#sentences = [s.strip() for s in sentences]
#for i in range(0,len(sentences)-1):
#  if(len(sentences[i]) == 0):
#     del sentences[i]


# 2.Summarize text with sumy

# In[24]:


#!pip install sumy
from sumy.parsers.plaintext import PlaintextParser #We're choosing a plaintext parser here, other parsers available for HTML etc.
from sumy.nlp.tokenizers import Tokenizer 
from sumy.summarizers.lsa import LsaSummarizer
from sumy.summarizers.text_rank import TextRankSummarizer #We're choosing Luhn, other algorithms are also built in
from sumy.summarizers.lex_rank import LexRankSummarizer #We're choosing Lexrank, other algorithms are also built in
from sumy.summarizers.luhn import LuhnSummarizer #We're choosing Luhn, other algorithms are also built in


# In[25]:


nltk.download('punkt')
#parser = PlaintextParser(sentences, Tokenizer("english"))
summarizer_lsa = LexRankSummarizer()


# In[26]:


parser = PlaintextParser.from_string(p,Tokenizer("english"))
summarizer_lsa = LexRankSummarizer()
##############################################################################################
import os       #importing os to set environment variable
### This part for buidling LDA Mallet Model
'''

def install_java():
    !apt-get install -y openjdk-8-jdk-headless -qq > /dev/null         
    os.environ["JAVA_HOME"] = "/usr/lib/jvm/java-8-openjdk-amd64"    
    !java -version       #check java version
install_java()


!wget http://mallet.cs.umass.edu/dist/mallet-2.0.8.zip
!unzip mallet-2.0.8.zip
'''

#os.environ["JAVA_HOME"] = "/usr/lib/jvm/java-8-openjdk-amd64"    
#os.environ.update({'MALLET_HOME':r'C:/Users/Pavithran/Desktop/Final Year Project/Flask Tests/FlaskServer/mallet-2.0.8'})
mallet_path = 'C:/mallet-2.0.8/bin/mallet'     ## may need to change in your local env
####

# Run in python console
import nltk; nltk.download('stopwords')
# Run in terminal or command prompt
#!python3 -m spacy download en
# 3.Summarize Text with genism
import re
import numpy as np
import pandas as pd
from pprint import pprint

# Gensim
import gensim
import gensim.corpora as corpora
from gensim.utils import simple_preprocess
from gensim.models import CoherenceModel

# spacy for lemmatization
import spacy

# Plotting tools
#!pip install pyLDAvis
import pyLDAvis
import pyLDAvis.gensim  # don't skip this
import matplotlib.pyplot as plt
#%matplotlib inline

# Enable logging for gensim - optional
import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.ERROR)

import warnings
warnings.filterwarnings("ignore",category=DeprecationWarning)
from nltk.corpus import stopwords
stop_words = stopwords.words('english')
stop_words.extend(['from', 'subject', 're', 'edu', 'use'])
from gensim.models.wrappers import LdaMallet
import os
nlp = spacy.load('en_core_web_sm', disable=['parser', 'ner'])

def compute_coherence_values(id2word,dictionary, corpus, texts, limit, start=2, step=3):
    """
    Compute c_v coherence for various number of topics

    Parameters:
    ----------
    dictionary : Gensim dictionary
    corpus : Gensim corpus
    texts : List of input texts
    limit : Max num of topics

    Returns:
    -------
    model_list : List of LDA topic models
    coherence_values : Coherence values corresponding to the LDA model with respective number of topics
    """
    coherence_values = []
    model_list = []
    for num_topics in range(start, limit, step):
        model = gensim.models.wrappers.ldamallet.LdaMallet(mallet_path, corpus=corpus,
                                                           num_topics=num_topics, id2word=id2word, random_seed=100)
        model_list.append(model)
        coherencemodel = CoherenceModel(model=model, texts=texts, dictionary=dictionary, coherence='c_v')
        coherence_values.append(coherencemodel.get_coherence())

    return model_list, coherence_values
def sent_to_words(sentence):
    yield(gensim.utils.simple_preprocess(str(sentence), deacc=True))  # deacc=True removes punctuations
# Define functions for stopwords, bigrams, trigrams and lemmatization
def remove_stopwords(texts):
    return [[word for word in simple_preprocess(str(doc)) if word not in stop_words] for doc in texts]

def make_bigrams(texts,bigram_mod):
    return [bigram_mod[doc] for doc in texts]

def lemmatization(texts, allowed_postags=['NOUN', 'ADJ', 'VERB', 'ADV']):
    """https://spacy.io/api/annotation"""
    texts_out = []
    for sent in texts:
        doc = nlp(" ".join(sent)) 
        texts_out.append([token.lemma_ for token in doc if token.pos_ in allowed_postags])
    return texts_out

def issue_num_tpm(data):
	# Remove Emails
    data = re.sub('\S*@\S*\s?', '', data)

    # Remove new line characters
    data = re.sub("\n", ' ', data) 

    # Remove distracting single quotes
    data = re.sub("\'", "", data)
    data_words = list(sent_to_words(data))
    # Build the bigram and trigram models
    bigram = gensim.models.Phrases(data_words, min_count=5, threshold=100) # higher threshold fewer phrases.
    trigram = gensim.models.Phrases(bigram[data_words], threshold=100)  

    # Faster way to get a sentence clubbed as a trigram/bigram
    bigram_mod = gensim.models.phrases.Phraser(bigram)
    trigram_mod = gensim.models.phrases.Phraser(trigram)
    # Remove Stop Words
    data_words_nostops = remove_stopwords(data_words)

    # Form Bigrams
    data_words_bigrams = make_bigrams(data_words_nostops,bigram_mod)

    # Initialize spacy 'en' model, keeping only tagger component (for efficiency)
    # python3 -m spacy download en
   

    # Do lemmatization keeping only noun, adj, vb, adv
    data_lemmatized = lemmatization(data_words_bigrams, allowed_postags=['NOUN', 'ADJ', 'VERB', 'ADV'])
    # Create Dictionary
    id2word = corpora.Dictionary(data_lemmatized)

    # Create Corpus
    texts = data_lemmatized

    # Term Document Frequency
    corpus = [id2word.doc2bow(text) for text in texts]
    #Building LDA Mallet Model
    model_list, coherence_values = compute_coherence_values(id2word=id2word,dictionary=id2word, corpus=corpus, texts=data_lemmatized, start=2, limit=15, step=2)
    limit=15; start=2; step=2;
    x = range(start, limit, step)
    max_index = coherence_values.index(max(coherence_values))
    return x[max_index]
##############################################################################################    
# In[28]:
from gensim.summarization import summarize
import pprint as ppr

def issue_generator(summarytext):
    '''
    Parameters
    ----------
    summarytext : String
        Summary of Text as generated by the summarizer_lsa function.

    Returns
    -------
    Predicted Issue title, Issue description, and keywords/tags for Issues.

    '''
    issue_tags = []
    issue = []
    for i in range(0,len(summarytext)):
        #print("Issues" + str(i+1) + ":")
        #print(summarytext[i],end="\n\n")
        tup = (title_create(str(summarytext[i])), str(summarytext[i]),key_word_func(summarytext[i]) )
        issue.append(tup)
    
    return issue    


'''
i_num = issue_num_tpm(p)
summary_2 =summarizer_lsa(parser.document,i_num) #Summarize the document with 7 sentences
for sentence in summary_2:
    print(sentence)
    print('\n\n')

a = issue_generator(summary_2)

summary = summarize(p)


print("Discussion link: https://docs.google.com/document/d/1R-fUKpPDsY7oIADHnr5k4fpzC2m1wG1mFLcqJEZd_gU/edit" + "\n")
print("Summary: \n" + summary +"\n")
print("Possible Issues: \n")
for i in range(0,len(summary_2)):
    print("Issues" + str(i+1) + ":")
    print(summary_2[i],end="\n\n")
    title_create(str(summary_2[i]))
    key_word_func(summary_2[i])
    print('\n')
'''
