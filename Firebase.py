# -*- coding: utf-8 -*-
"""
Created on Sun Jul 19 05:32:20 2020

@author: Pavithran
"""


import firebase_admin
from firebase_admin import credentials

from firebase_admin import firestore

cred = credentials.Certificate("fyp-ai-firebase-adminsdk-vnadt-a46dfc3e03.json")
#firebase_admin.initialize_app(cred)

db = firestore.client()


doc_ref = db.collection(u'users').document(u'alovelace')
doc_ref.set({
    u'first': u'Ada',
    u'last': u'Lovelace',
    u'born': 1815
})

import JiraAPi as doc_obtainer

doc_ref = db.collection(u'Confluence').document(u'Content')
doc_ref.set(doc_obtainer.Confluence_content)
doc_ref = db.collection(u'Google Content').document(u'Content')
doc_ref.set(doc_obtainer.google_docs_content)