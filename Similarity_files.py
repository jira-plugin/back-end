
#------------------------------------------------------------------------#
#Issue acquisition
#from __future__ import print_function
#from atlassian import Jira

#jira = Jira('https://jira.sakaiproject.org')

#all_projects = jira.get_all_projects(included_archived=None)
#projects_components = jira.get_project_components('SMP')
#projects_list = []

#Obtains keys of all projects present in a particular JIRA
#projects = jira.projects()
#keys = sorted([project['key'] for project in projects])

#issue_keys = 'SAK-43199'
#issues = jira.issue(issue_keys)

#descriptions_keys = {}

#Obtain the issue, and their descriptions.
#issue_keys = [issue['key'] for issue in issues]
#descriptions = [issue['fields']['description'] for issue in issues] 
#summary = [issue['fields']['description'] for issue in issues] 

#description = issues['fields']['description']  #Description of the issue.
#summary = issues['fields']['summary']
#count = 0
#for l in issue_keys:
#    descriptions_keys.update({issue_keys[count]:descriptions[count]})
#    count += 1
    
    
#End of issues acqusition
#------------------------------------------------------------------------#

    

from gensim import corpora, models, similarities
import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

#Obtain the Confluence 
import os
import sys
import nltk
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer

import sqlite3 as sq
conn = sq.connect('AIAN_database.sqlite')
cur = conn.cursor()
cur.execute('SELECT * FROM confluence')
confluence_data = cur.fetchall()
cur.execute('SELECT * FROM google')
google_data = cur.fetchall()

sql_command = """
DROP TABLE IF EXISTS simresults;
CREATE TABLE simresults (
    title INTEGER,
    link VARCHAR,
    content VARCHAR);
"""
cur.executescript(sql_command)
conn.commit()

def obtain_similarities(description):
    dictionary = corpora.Dictionary.load('AIAN_dict.dict')
    corpus = corpora.MmCorpus('AIAN_corpus.mm')
    tfidf = models.TfidfModel(corpus)
    corpus_tfidf = tfidf[corpus]
    lsi_model = models.LsiModel(corpus_tfidf, id2word=dictionary, num_topics=10)
    corpus_lsi = lsi_model[corpus_tfidf]
    
    index = similarities.MatrixSimilarity(lsi_model[corpus])

    en_str = description
    en_str_vec = dictionary.doc2bow(en_str.lower().split())
    #print(en_str)
    #print(en_str_vec)

    lsi_str_vec1 = lsi_model[en_str_vec]
    sims = index[lsi_str_vec1]
    simsorted = sorted(enumerate(sims), key=lambda item: -item[1])
    
    return simsorted
    #print (simsorted)

documents = []
links = []
titles = []
count = 0
while count < len(confluence_data):
    titles.append(confluence_data[count][0])
    documents.append(confluence_data[count][1])
    links.append(confluence_data[count][2])
    count += 1

count = 0
while count < len(google_data):
    titles.append(google_data[count][0])
    documents.append(google_data[count][1])
    count+=1
   
 
#Obtain the issues from the JiraIssues module.
#Will change into a function in the future.
import JiraIssues as issue_obtainer
#Hard coded - Need to make it dynamic


def similarities_func(issue_key,documents,titles):
    best_sim = {}
    simpercent = {}
    bestsim_link = {}
    bestsim_title = {}
    summar_sim = {}
    main_count = 0
    simsorted_separate = {}
    description = issue_obtainer.obtain_issues(issue_key) 
    data2 = cur.fetchall()
    simsorted_separate = obtain_similarities(description)
    count = 0
    chosen_doc =[]
    chosen_links = []
    chosen_title = []
    cur.execute('DELETE FROM simresults')
    conn.commit()
    while count <= 5:
        dockey = simsorted_separate[count][0]
        chosen_doc.append(documents[dockey])
        chosen_title.append(titles[dockey])
        chosen_links.append(links[dockey])
        simpercent.update({issue_key:simsorted_separate[count][1]})
        best_sim.update({issue_key:chosen_doc})
        bestsim_link.update({issue_key:chosen_links})
        bestsim_title.update({issue_key:chosen_title})
        cur.execute('INSERT INTO simresults (title, link,content) values (?,?,?)', (titles[dockey], documents[dockey],links[dockey] ))
        conn.commit()
        count+=1
    
    import Summarizer
    summarized = Summarizer.summary_list
    #print(summarized)
    summar_sim.update({issue_key:summarized})
    highest_percentage = simpercent[issue_key]
    

    return simpercent,best_sim,bestsim_link, bestsim_title, summar_sim
    

'''
issue_key = "SAK-4319"
high, best, link , title, summar= similarities_func(issue_key, documents,titles)
cur.execute('SELECT * FROM simresults')
data2 = cur.fetchall()
'''
 
