# -*- coding: utf-8 -*-
"""
Created on Tue Sep 15 12:28:03 2020

@author: Pavithran
"""

import nltk
nltk.download('stopwords')

import re
import numpy as np
import pandas as pd
from pprint import pprint
# Gensim
import gensim
import gensim.corpora as corpora
from gensim.utils import simple_preprocess
from gensim.models import CoherenceModel

# spacy for lemmatization
import spacy

# Plotting tools
import pyLDAvis
import pyLDAvis.gensim  # don't skip this
import matplotlib.pyplot as plt
#%matplotlib inline

# Enable logging for gensim - optional
import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.ERROR)

import warnings
warnings.filterwarnings("ignore",category=DeprecationWarning)

# NLTK Stop words
from nltk.corpus import stopwords
stop_words = stopwords.words('english')
stop_words.extend(['from', 'subject', 're', 'edu', 'use'])
from gensim.models.wrappers import LdaMallet
import os

nlp = spacy.load('en_core_web_sm', disable=['parser', 'ner'])

def sent_to_words(sentence):
    yield(gensim.utils.simple_preprocess(str(sentence), deacc=True))  # deacc=True removes punctuations

os.environ.update({'MALLET_HOME':r'C:/mallet-2.0.8/'})

mallet_path = 'C:/mallet-2.0.8/bin/mallet'  

def compute_coherence_values(id2word,dictionary, corpus, texts, limit, start, step):
    """
    Compute c_v coherence for various number of topics

    Parameters:
    ----------
    dictionary : Gensim dictionary
    corpus : Gensim corpus
    texts : List of input texts
    limit : Max num of topics

    Returns:
    -------
    model_list : List of LDA topic models
    coherence_values : Coherence values corresponding to the LDA model with respective number of topics
    """
    coherence_values = []
    model_list = []
    for num_topics in range(start, limit, step):
        print("--")
        model = gensim.models.wrappers.ldamallet.LdaMallet(mallet_path, corpus=corpus,
                                                           num_topics=num_topics, id2word=id2word)
        print("---")
        model_list.append(model)
        print("----")
        coherencemodel = CoherenceModel(model=model, texts=texts, dictionary=dictionary, coherence='c_v')
        print("-----")
        coherence_values.append(coherencemodel.get_coherence())
        print("-------")

    return model_list, coherence_values

# Define functions for stopwords, bigrams, trigrams and lemmatization
def remove_stopwords(texts):
    return [[word for word in simple_preprocess(str(doc)) if word not in stop_words] for doc in texts]

def make_bigrams(texts,bigram_mod):
    return [bigram_mod[doc] for doc in texts]

def lemmatization(texts, allowed_postags=['NOUN', 'ADJ', 'VERB', 'ADV']):
    """https://spacy.io/api/annotation"""
    texts_out = []
    for sent in texts:
        doc = nlp(" ".join(sent)) 
        texts_out.append([token.lemma_ for token in doc if token.pos_ in allowed_postags])
    return texts_out

def issue_num_tpm(data):
	# Remove Emails
    #print(data)
    data = re.sub('\S*@\S*\s?', '', data)

    # Remove new line characters
    data = re.sub("\n", ' ', data) 

    # Remove distracting single quotes
    data = re.sub("\'", "", data)
    data_words = list(sent_to_words(data))
    # Build the bigram and trigram models
    bigram = gensim.models.Phrases(data_words, min_count=5, threshold=100) # higher threshold fewer phrases.
    trigram = gensim.models.Phrases(bigram[data_words], threshold=100)  
    #print('1-')
    # Faster way to get a sentence clubbed as a trigram/bigram
    bigram_mod = gensim.models.phrases.Phraser(bigram)
    trigram_mod = gensim.models.phrases.Phraser(trigram)
    # Remove Stop Words
    data_words_nostops = remove_stopwords(data_words)
    #print('1--')
    # Form Bigrams
    data_words_bigrams = make_bigrams(data_words_nostops,bigram_mod)
    #print('1---')
    # Initialize spacy 'en' model, keeping only tagger component (for efficiency)
    # python3 -m spacy download en
   

    # Do lemmatization keeping only noun, adj, vb, adv
    data_lemmatized = lemmatization(data_words_bigrams, allowed_postags=['NOUN', 'ADJ', 'VERB', 'ADV'])
    # Create Dictionary
    id2word = corpora.Dictionary(data_lemmatized)
    #print('1----')
    # Create Corpus
    texts = data_lemmatized
    #print('1-----')
    # Term Document Frequency
    corpus = [id2word.doc2bow(text) for text in texts]
    #print('1------')
    #Building LDA Mallet Model
    model_list, coherence_values = compute_coherence_values(id2word=id2word,dictionary=id2word, corpus=corpus,
                                                            texts=data_lemmatized,limit = 5, start = 2, step = 2)
    print("1")
    print(model_list)
    print("1")
    print(coherence_values)
    print("1")
    #print('1-------')
    limit=15; start=2; step=2;
    x = range(start, limit, step)
    print(x)
    #print('1--------')
    max_index = coherence_values.index(max(coherence_values))
    print(max_index)
    #print('1---------')
    return x[max_index]


p = '''
A choice often available in other gradebook tools is whether the student grade calculation uses a running total 
or not.  In Sakai's Gradebook 1.0, there is no choice -- it's simply a Running Total.  
While there is now an optional feature to display the overall total points (SAK-18588) that a student 
as earned out of the overall total points possible, the grade calculation is still a running total. 
 Several professors at our institution have requested that this be an option that they can choose, 
 to avoid student confusion.
'''
print(p)
i_num = issue_num_tpm(p)
