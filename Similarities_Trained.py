# -*- coding: utf-8 -*-
"""
Created on Wed Sep 16 02:12:21 2020

@author: Pavithran
"""


import sqlite3 as sq
from gensim import corpora, models, similarities
import requests
from requests.auth import HTTPBasicAuth
import scipy
import JiraIssues as issues
import Similarities2 as f1

conn = sq.connect('AIAN_database.sqlite')
cur = conn.cursor()


cur.execute('SELECT * FROM confluence')
confluence_data = cur.fetchall()
cur.execute('SELECT * FROM google')
google_data = cur.fetchall()
cur.execute('SELECT * FROM issues')
issuedata = cur.fetchall()
issuedata = issuedata[3000:]
documents = []
links = []
title = []
count = 0
while count < len(confluence_data):
    title.append(confluence_data[count][0])
    documents.append(confluence_data[count][1])
    links.append(confluence_data[count][2])
        
    count += 1

count = 0
while count < len(google_data): 
    title.append(google_data[count][0])
    documents.append(google_data[count][1])
    count+=1


from sentence_transformers import SentenceTransformer, SentencesDataset, InputExample, losses
from transformers import AutoTokenizer, AutoModel
#tokenizer = AutoTokenizer.from_pretrained("sentence-transformers/bert-base-nli-mean-tokens")
#model = AutoModel.from_pretrained("sentence-transformers/bert-base-nli-mean-tokens")
#tokenizer.save_pretrained('./local_directory/')
#model.save_pretrained('./local_directory/')
model = SentenceTransformer('distilbert-base-nli-stsb-mean-tokens')
train_examples = [InputExample(texts=['My first sentence', 'My second sentence'], label=0.8),
    InputExample(texts=['Another pair', 'Unrelated sentence'], label=0)]
training_dataset  = []
database_dataset = {}
#dataset = SentencesDataset(train_examples,model )
sql_command = """
DROP TABLE IF EXISTS training_dataset;
CREATE TABLE training_dataset (
    description VARCHAR,
    content VARCHAR,
    label VARCHAR);
"""
cur.executescript(sql_command)
conn.commit()
for i in range(0,600):
    description = issuedata[i][1]
    simresults = f1.Similairties(description)
    '''
    x = 0
    training_dataset.append([InputExample(texts=[description,documents[simresults[x][0]]], label=0.8),
                            InputExample(texts=[description,documents[simresults[x+1][0]]], label=0.6),
                            InputExample(texts=[description,documents[simresults[x+2][0]]], label=0.4),
                            InputExample(texts=[description,documents[simresults[x+3][0]]], label=0.2),
                            InputExample(texts=[description,documents[simresults[x+4][0]]], label=0)]) 
    '''
    doc_labelpair = {}
    for x in range(0,5):
        cur.execute('INSERT INTO training_dataset (description, content,label) values (?,?,?)',
                    (description,documents[simresults[x][0]],(0.8-(x*0.2))  ))
        conn.commit()

        
#database_dataset.update({description:doc_labelpair})
for sentence_data in training_dataset:
    dataset = SentencesDataset(sentence_data,model )

cur.execute('SELECT * FROM training_dataset')
test_data = cur.fetchall()
conn.close()
#dataset = SentencesDataset(training_dataset[0],model )
#dataset = SentencesDataset(training_dataset[1],dataset )
