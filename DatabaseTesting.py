# -*- coding: utf-8 -*-
"""
Created on Mon Jul 27 05:02:05 2020

@author: Pavithran
"""
import sqlite3 as sq
#import Similarity_files as sims
conn = sq.connect('AIAN_database.sqlite')
cur = conn.cursor()

googlebaseurl = 'https://docs.google.com/document/d/'
cur.execute('SELECT * FROM confluence')
confluence_data = cur.fetchall()
cur.execute('SELECT * FROM google')
google_data = cur.fetchall()
cur.execute('SELECT * FROM simresults')
issuedata = cur.fetchall()
import html2text as h2t
h = h2t.HTML2Text()
h.unicode_snob = True
documents = []
links = []
title = []
bstset = []
issuedesc= {}
count = 0
while count < len(confluence_data):
    title.append(confluence_data[count][0])
    documents.append(confluence_data[count][1])
    links.append(confluence_data[count][2])
    s = (links[count], h.handle(confluence_data[count][1]))
    bstset.append(s)
        
    count += 1

count = 0
while count < len(google_data): 
    title.append(google_data[count][0])
    documents.append(google_data[count][1])
    links.append(googlebaseurl + google_data[count][2])
    s = (links[count], h.handle(google_data[count][1]))
    bstset.append(s)
    count+=1
count = 0
while count<len(issuedata):
    issuedesc.update({issuedata[count][0]:issuedata[count][1]})
    count+=1
    
conn.close()