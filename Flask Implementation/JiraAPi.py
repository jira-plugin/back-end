from __future__ import print_function
from atlassian import Jira
from atlassian import Confluence
import re
from bs4 import BeautifulSoup
from urllib.parse import urlparse
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from googleapiclient.errors import HttpError as GoogleHttpError


import JiraIssues as issues
confluence = Confluence("https://confluence.sakaiproject.org")


# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/documents.readonly']

# The ID of the document for SAK-42942, we will change this to obtain different files.
# Currently being done manually, will need a plan to get this manually.
DOCUMENT_ID = '1R-fUKpPDsY7oIADHnr5k4fpzC2m1wG1mFLcqJEZd_gU'

#----Variables-----------#
#Get the content of a page, based on the id of the page.
#We use this pattern to extract just the document Id
google_doc_pattern = "d\/(.*?)\/edit"
confluence_pattern = "display\/(.*?)\/"
google_docs = []
google_docs_content = {
    }
google_docs_title =[]
Document_ids = []
All_links = []
Confluence_links = []
Text_with_links = {
    }
Confluence_titles = []
Confluence_content = {}
main_count = 0
count = 0
doccount = 0
#----Google API----------#
def read_paragraph_element(element):
    text_run = element.get('textRun')
    if not text_run:
        return ''
    return text_run.get('content')


def Url_parser(element):
    for y in element:
        parsedUrl = urlparse(str(element))
        substring = re.search(google_doc_pattern,str(parsedUrl)).group(1)
    return substring

def read_strucutural_elements(elements):
  
    text = ''
    for value in elements:
        if 'paragraph' in value:
            elements = value.get('paragraph').get('elements')
            for elem in elements:
                text += read_paragraph_element(elem)
        elif 'table' in value:
            # The text in table cells are in nested Structural Elements and tables may be
            # nested.
            table = value.get('table')
            for row in table.get('tableRows'):
                cells = row.get('tableCells')
                for cell in cells:
                    text += read_strucutural_elements(cell.get('content'))
        elif 'tableOfContents' in value:
            # The text in the TOC is also in a Structural Element.
            toc = value.get('tableOfContents')
            text += read_strucutural_elements(toc.get('content'))
    return text


def Googledocsretrieval():
 
    #This is the code for credentials, I have currently configured it for my account.
    #But we will have to fix this once we start the actual project.
    creds = None
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:	
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('docs', 'v1', credentials=creds)

    # Retrieve the documents contents from the Docs service.
    document = service.documents().get(documentId=DOCUMENT_ID).execute()
    document_content = document.get('body').get('content')
    print(read_strucutural_elements(document_content))
   
    #file =  open("google_doc_content.txt","a")
    #file.write(document.get('title') + ' : ' + google_docs[count] )
    #file.write(read_strucutural_elements(document_content))
    google_docs_content.update({document.get('title'):read_strucutural_elements(document_content)})
    google_docs_title.append(document.get('title'))
    #file.close()
    return google_docs_content,google_docs_title
    
#---Confluence----#
#Function to store all page ids in a list for usage later.
ids_pages = []
def store_listof_pages(element):
    iter = 0
    iter2 = 0
    for something in element:
        #print(conf_data[iter]['id'])
        #ids_pages.append(element[iter]['id'])        
        ind_spaces = element[iter]
        iter += 1
        iter2 = 0
        for another in ind_spaces:
            ids_pages.append(ind_spaces[iter2]['id'])
            iter2 += 1
    return ids_pages

   
  
ConfluenceSpaces = []

ConfluenceSpaces.append(confluence.get_all_spaces(start=0, limit=5, expand=None))


Confluence_keys = []
#Store all the confluence spaces' key in a list.
keys = []
def conf_spaces(element):
    for something in element:
        for anotherthing in something:
            keys.append(anotherthing['key'])
    return keys    

Confluence_keys = conf_spaces(ConfluenceSpaces)   
conf_data = []

confluence.get_all_spaces(start=0, limit=500, expand=None)
def obtain_confluence(issue_key):
    #description, issue = issues.obtain_issues(issue_key)
    #key = issue['fields']['project']['key']
    #creator_key = issue['fields']['creator']['key']
    #reporter_key = issue['fields']['reporter']['key']
    for a in Confluence_keys:
            conf_data.append(confluence.get_all_pages_from_space(a, start=0, limit=10, status=None, expand=None, content_type='page'))

  
    
    return conf_data
    

data = obtain_confluence('GRBK-4')
id_data = store_listof_pages(data)




for element in id_data:
    page_data = confluence.get_page_by_id(id_data[main_count],expand='body.storage')
    baselink = page_data['_links']['base']
    weblink = page_data['_links']['webui']
    link = baselink + weblink
    content=page_data['body']['storage']['value']
    title = page_data['title']
    Confluence_titles.append(title) # The title of confluence page is placed in this list.
    #file = open('confluence_titles.txt','a')
    #file.write(page_data['id'] + ' : Title: ' + title)
    #file.close
    Confluence_content.update({title:content}) #The content is placed in this list. Print it or use the variable explorer in Spyder to use it.
    Text_with_links.update({link:content})
    soup = BeautifulSoup(content, 'html.parser')
    for all_links in soup.find_all('a'):
        
        try:
            All_links.append(all_links['href'])
            
        
        except KeyError:
            print('No link found ')
            continue
               
    for con_links in soup.find_all('a',attrs ={'href':re.compile('confluence')}):
        Confluence_links.append(con_links['href']) #The confluence links are stored in this list, print it or use the variable explorer in Spyder to use it.
    for para in soup.find_all('p'):
         print(para.text)
    #We then retrieve all google doc links from the particular page.
    try:
        for link in soup.find_all('a',attrs = {'href':re.compile('google.com/document')}):
        #print(link['href'])
            google_docs.append(link['href']) #The google doc links are placed in this list.  Print it or use the variable explorer in Spyder to use it.
            try:
                doc_id = Url_parser(google_docs[count])
            
            except AttributeError:
                print('No link found')
                continue
            
            Document_ids.append(doc_id)
            DOCUMENT_ID = Document_ids[count]
            googledocs, googletitles = Googledocsretrieval()
            count += 1
    except GoogleHttpError:
           print('We don\'t have access to this file. ')
           count += 1
    
    main_count += 1

'''
for a in Confluence_keys:
    Confluence_space = a
    conf_data.append(confluence.get_all_pages_from_space(Confluence_space, start=0, limit=1000, status=None, expand=None, content_type='page'))

    
    
Confluence_space = 'SG2X'
#conf_data.append(confluence.get_all_pages_from_space(Confluence_space, start=0, limit=1000, status=None, expand=None, content_type='page'))
conf_data = confluence.get_all_pages_from_space(Confluence_space, start=0, limit=1000, status=None, expand=None, content_type='page')
#Store Json data of all pages from a space.


#Get the total number of pages in a particular space.
#For the purposes of testing, I am using one particular space, but it should be possibe,
#for all spaces in a Confluence page.
list_ofpages = []
list_ofpages = store_listof_pages(conf_data)
       

#---------Main---------#

#------Miscelaneous Functions-------#


    for para in soup.find_all('p'):
         print(para.text)
         file =  open("confluence_content.txt","a")
         file.write( page_data['id'] + ' : Title: ' + title)
         file.write(para.text)


'''