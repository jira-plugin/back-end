from __future__ import print_function
from atlassian import Jira
from atlassian import Confluence
import re
from bs4 import BeautifulSoup
from urllib.parse import urlparse
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from googleapiclient.errors import HttpError as GoogleHttpError
jira = Jira('https://jira.sakaiproject.org')
all_projects = jira.get_all_projects(included_archived=None)
#projects_components = jira.get_project_components('BBB')
projects_list = []

#Obtains keys of all projects present in a particular JIRA
projects = jira.projects()
keys = sorted([project['key'] for project in projects])

def obtain_issues(issue_key):
    issues = jira.get_all_project_issues('GRBK')
    descriptions_keys = {
    "Key" : 'key',
    "Description" : 'description'}
    issue_keys = [issue['key'] for issue in issues]
    descriptions = [issue['fields']['description'] for issue in issues] 
    summary = [issue['fields']['summary'] for issue in issues] 
    creator = [issue['fields']['creator']['displayName'] for issue in issues] 
    created = [issue['fields']['created'] for issue in issues] 
    reporter = [issue['fields']['reporter']['displayName']for issue in issues] 
    count = 0
    for l in issue_keys:
        descriptions_keys.update({issue_keys[count]:descriptions[count]})
        count += 1
        
    spec_issue = jira.issue(issue_key)
    description = str(issue_key)
    description += "\n"
    description += str(spec_issue['fields']['description'])
    description += "\n"
    description += str(spec_issue['fields']['summary'])
    description += "\n"
    description += str(spec_issue['fields']['creator']['displayName'])
    description += "\n"
    description += str(spec_issue['fields']['created'])
    description += "\n"
    description += str(spec_issue['fields']['reporter']['displayName'])
    
    return description
            
    

#x = obtain_issues('GRBK-5')    
#print(x)






        