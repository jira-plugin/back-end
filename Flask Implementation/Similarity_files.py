
#------------------------------------------------------------------------#
#Issue acquisition
#from __future__ import print_function
#from atlassian import Jira

#jira = Jira('https://jira.sakaiproject.org')

#all_projects = jira.get_all_projects(included_archived=None)
#projects_components = jira.get_project_components('SMP')
#projects_list = []

#Obtains keys of all projects present in a particular JIRA
#projects = jira.projects()
#keys = sorted([project['key'] for project in projects])

#issue_keys = 'SAK-43199'
#issues = jira.issue(issue_keys)

#descriptions_keys = {}

#Obtain the issue, and their descriptions.
#issue_keys = [issue['key'] for issue in issues]
#descriptions = [issue['fields']['description'] for issue in issues] 
#summary = [issue['fields']['description'] for issue in issues] 

#description = issues['fields']['description']  #Description of the issue.
#summary = issues['fields']['summary']
#count = 0
#for l in issue_keys:
#    descriptions_keys.update({issue_keys[count]:descriptions[count]})
#    count += 1
    
    
#End of issues acqusition
#------------------------------------------------------------------------#

    

from gensim import corpora, models, similarities
import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

#Obtain the Confluence 
import os
import sys
sys.path.append('C:/Users/Pavithran/Desktop/Final Year Project/Obtaining a bigger dataset/JiraAPi.py')
import JiraAPi as JiraCust
import nltk
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer


def obtain_similarities(description, documents):
    #Clean the data 
    texts = [[word for word in document.lower().split()] for document in documents]

    nltk.download('punkt')

    texts_tokenized = [[word.lower() for word in word_tokenize(document)] for document in documents]
    #print(texts_tokenized)
    
 
    nltk.download('stopwords')
    english_stopwords = stopwords.words('english')
    texts_filtered_stopwords = [[word for word in document if not word in english_stopwords] for document in texts_tokenized]
    #print(texts_filtered_stopwords)
    english_punctuations = [',','.',':',';','?','!','(',')','[',']','@','&','#','%','$','{','}','--','-','’','“'
                            ,'”']
    texts_filtered = [[word for word in document if not word in english_punctuations] for document in texts_filtered_stopwords]
    
   
    st = PorterStemmer()   
    texts_stemmed = [[st.stem(word) for word in document] for document in texts_filtered]

    all_stems = sum(texts_stemmed,[])
    stems_once = set(stem for stem in set(all_stems) if all_stems.count(stem) == 1)
    texts_stemmed = [[stem for stem in text if stem not in stems_once] for text in texts_stemmed]  

    #creating dictonary(Bow)
    dictionary = corpora.Dictionary(texts_stemmed)
    corpus = [dictionary.doc2bow(text) for text in texts_stemmed]
    
    #print(dictionary.token2id)
    #print(corpus)
    
    tfidf = models.TfidfModel(corpus)
    corpus_tfidf = tfidf[corpus]
    lsi_model = models.LsiModel(corpus_tfidf, id2word=dictionary, num_topics=10)
    corpus_lsi = lsi_model[corpus_tfidf]
    
    index = similarities.MatrixSimilarity(lsi_model[corpus])

    en_str = description
    en_str_vec = dictionary.doc2bow(en_str.lower().split())
    #print(en_str)
    #print(en_str_vec)

    lsi_str_vec1 = lsi_model[en_str_vec]
    sims = index[lsi_str_vec1]
    simsorted = sorted(enumerate(sims), key=lambda item: -item[1])
    
    return simsorted
    #print (simsorted)





#Confluence_content = JiraCust.Confluence_content
google_content = JiraCust.google_docs_content
docs_with_links = JiraCust.Text_with_links
google_links = JiraCust.google_docs
doc_titles = JiraCust.Confluence_titles
documents = []
links = []
#for x,y in Confluence_content.items():
#    documents.append(y)
    

for x,y in docs_with_links.items():
    documents.append(y)
    links.append(x)
    
for x,y in google_content.items():
    documents.append(y)
    
if google_links:
    links.append(google_links)
    

   

   
#Obtain the issues from the JiraIssues module.
#Will change into a function in the future.
import JiraIssues as issue_obtainer
from bs4 import BeautifulSoup

best_sim = {}
simpercent = {}
bestsim_link = {}
bestsim_title = {}
main_count = 0

def similarities_func(issue_key,documents):
    description = issue_obtainer.obtain_issues(issue_key) 
    simsorted_separate = obtain_similarities(description, documents)
    count = 0
    chosen_doc =[]
    chosen_links = []
    chosen_title = []

    while count <= 5:
        dockey = simsorted_separate[count][0]
        chosen_doc.append(documents[dockey])
        chosen_links.append(links[dockey])
        chosen_title.append(doc_titles[dockey])
        simpercent.update({issue_key:simsorted_separate[count][1]})
        best_sim.update({issue_key:chosen_doc})
        bestsim_link.update({issue_key:chosen_links})
        bestsim_title.update({issue_key:chosen_title})
        count+=1
    
    highest_percentage = simpercent[issue_key]
    return highest_percentage,best_sim,bestsim_link, bestsim_title
    

high, best,link,title = similarities_func("GRBK-4",documents)
    
    
    
    


#summarization
from pprint import pprint as print
from gensim.summarization import summarize
path = r'C:\Users\Pavithran\Desktop\Final Year Project\Obtaining a bigger dataset\corpusdata\test.txt'
with open(path) as file:
    text = file.read()
#print(summarize(text))
#print(summarize(text, split=True))
#You can adjust how much text the summarizer outputs via the “ratio” parameter or the “word_count” parameter. Using the “ratio” parameter, you specify what fraction of sentences in the original text should be returned as output. Below we specify that we want 50% of the original text (the default is 20%).
#print(summarize(text, ratio=0.5))
#print(summarize(text, word_count=50))
from gensim.summarization import keywords
#print(keywords(text))
#print(text)
