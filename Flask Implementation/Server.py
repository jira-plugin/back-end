# -*- coding: utf-8 -*-
"""
Created on Mon Jun  1 21:53:10 2020

@author: Pavithran
"""



try:
    from flask import Flask, render_template, url_for, jsonify,redirect,send_from_directory,request,Blueprint
    from flask_ngrok import run_with_ngrok
    from flask_atlassian_connect import AtlassianConnect
    import os, json
    import JiraIssues as issues
    import JiraAPi as doc_obtainer
    import Similarity_files as sims
    from bs4 import BeautifulSoup
    import html2text

    count = 0
    #sim_list = sims.simsorted
    #docs = sims.documents
   # sim5 = []
    
    app = Flask(__name__, static_url_path='')
    ac = AtlassianConnect(app)
    app.config['JSONIFY_PRETTYPRINT_REGULAR'] = False
    SITE_ROOT = os.path.realpath(os.path.dirname(__file__))
    filename = os.path.join(SITE_ROOT, "atlassian-connect.json")  
    
   
    #For testing purposes, creates a url for Testing the REST API function.
    google_content = doc_obtainer.google_docs_content
    docs_with_links = doc_obtainer.Text_with_links
    google_links = doc_obtainer.google_docs
    doc_titles = doc_obtainer.Confluence_titles
    documents = []
    links = []
#for x,y in Confluence_content.items():
#    documents.append(y)
    

    for x,y in docs_with_links.items():
        documents.append(y)
        links.append(x)
    
    for x,y in google_content.items():
        documents.append(y)
    
    if google_links:
        links.append(google_links)
    
    @app.route("/<string:issuekey>", methods=['POST', 'GET'])
    def hello(issuekey):
        description = issues.obtain_issues(issuekey)
        highest_percentage,best_sim,best_links,best_titles = sims.similarities_func(issuekey, documents)
        
        return jsonify(best_sim)
   
    #http://127.0.0.1:5000/atconnect
    @app.route("/atconnect", methods = ['GET','POST'])
    def atconnect():
        return redirect(url_for('static', filename='atlassian-connect.json'))

    if __name__ == '__main__':
        app.run()
    
except TimeoutError:
          print('We don\'t have access to this file. ')
          count += 1
    


