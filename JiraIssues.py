from __future__ import print_function
from atlassian import Jira
from jira import JIRA
from atlassian import Confluence
import re
from bs4 import BeautifulSoup
from urllib.parse import urlparse
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from googleapiclient.errors import HttpError as GoogleHttpError

#all_projects = jira.get_all_projects(included_archived=None)
#projects_components = jira.get_project_components('BBB')
projects_list = []

import sqlite3 as sq
conn = sq.connect('IssueDatabase.sqlite')
cur = conn.cursor()


#jira = Jira('https://jira.sakaiproject.org')
#jira1 = JIRA('https://jira.sakaiproject.org')
url = 'https://atc3360.atlassian.net'
options = {"server": url}
user = 'pm508@uowmail.edu.au'
API = 'dPoTcuZRbBMrKf4HXjyXFCAF'
#jira = JIRA(options, basic_auth=(user,API) )
jira = Jira(url, user, API)
   
'''
     

sql_command = """
DROP TABLE IF EXISTS issues;
CREATE TABLE issues (
    key INTEGER,
    desc VARCHAR,
    created VARCHAR,
    updated VARCHAR
    );
"""
cur.executescript(sql_command)
conn.commit()

block_size = 100
block_num = 0
while True:
  start_idx = block_num * block_size
  if block_num == 0:
    issues = jira1.search_issues("", start_idx, block_size)
  else:
    more_issue = jira1.search_issues("", start_idx, block_size)
    if len(more_issue)>0:
      for x in more_issue:
        issues.append(x)
        
    else:
      break
  if len(issues) ==  0:
    # Retrieve issues until there are no more to come
    break
  block_num += 1

print(len(issues))

for issue in issues:
    try:
        print('{}: {} :{}:{}:{}:{}:{}'.format(issue.key, issue.fields.summary, issue.fields.description
                                        ,issue.fields.reporter.displayName,issue.fields.creator.displayName
                                        , issue.fields.updated, issue.fields.created)) 
        description = str(issue.key)
        description += "\n"
        description += str(issue.fields.summary)
        description += "\n"
        description += str(issue.fields.description)
        description += "\n"
        description += str(issue.fields.reporter.displayName)
        description += "\n"
        description += str(issue.fields.creator.displayName)
        description += "\n"
        description += str(issue.fields.reporter.displayName)
        cur.execute('INSERT INTO issues (key, desc,created, updated ) values (?,?,?,?)' , (issue.key, description
                                                                                           ,issue.fields.created
                                                                                           ,issue.fields.updated
                                                                                           ))
        conn.commit()
    
    except AttributeError:
        print("None Attribute Found")
        continue
'''
cur.execute('SELECT * FROM issues')
issue_data = cur.fetchall()    
    


#Obtains keys of all projects present in a particular JIRA

documents = []
links = []
keys = []
count = 0
while count < len(issue_data):
    keys.append(issue_data[count][0])
    count += 1

issue_descriptions = {}
def obtain_all():
    counts = 0
    while counts<len(issue_data):
        desc = obtain_issues(keys[counts])
        issue_descriptions.update({keys[counts]:desc})
        cur.execute('INSERT INTO issuedesc (key, desc) values (?,?)', (keys[counts], desc))
        conn.commit()
        print(desc)
        counts += 1

def obtain_issues(issue_key):
    '''
    issues = jira.get_all_project_issues('GRBK')
    descriptions_keys = {
    "Key" : 'key',
    "Description" : 'description'}
    issue_keys = [issue['key'] for issue in issues]
    descriptions = [issue['fields']['description'] for issue in issues] 
    summary = [issue['fields']['summary'] for issue in issues] 
    creator = [issue['fields']['creator']['displayName'] for issue in issues] 
    created = [issue['fields']['created'] for issue in issues] 
    reporter = [issue['fields']['reporter']['displayName']for issue in issues] 
    count = 0
    for l in issue_keys:
        descriptions_keys.update({issue_keys[count]:descriptions[count]})
        count += 1
    ''' 
    spec_issue = jira.issue(issue_key)
    x = {}  
    description = str(issue_key)
    
    description += "\n"
    description += str(spec_issue['fields']['description'])
    description += "\n"
    x.update({'description':str(spec_issue['fields']['description'])})
    description += str(spec_issue['fields']['summary'])
    description += "\n"
    x.update({'summary':str(spec_issue['fields']['summary'])})
    description += str(spec_issue['fields']['creator']['displayName'])
    description += "\n"
    description += str(spec_issue['fields']['created'])
    description += "\n"
    description += str(spec_issue['fields']['reporter']['displayName'])
    
    return description
            

'''
a = obtain_issues('DEC1312-76')
issue_creator = JIRA(options, basic_auth=(user,API) )
def issuegen(content):
    content.update({'project': {'id': 10000}})
    content.update({'issuetype': {'name': 'Bug'}})
    new_issue = issue_creator.create_issue(fields=content)
    
    return new_issue
'''

