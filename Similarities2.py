#!/usr/bin/env python
# coding: utf-8

# In[33]:


import sqlite3 as sq
from gensim import corpora, models, similarities
import requests
from requests.auth import HTTPBasicAuth
conn = sq.connect('AIAN_database.sqlite')
cur = conn.cursor()


# In[34]:


cur.execute('SELECT * FROM confluence')
confluence_data = cur.fetchall()
cur.execute('SELECT * FROM google')
google_data = cur.fetchall()


documents = []
links = []
title = []
count = 0
while count < len(confluence_data):
    title.append(confluence_data[count][0])
    documents.append(confluence_data[count][1])
    links.append(confluence_data[count][2])
        
    count += 1

count = 0
while count < len(google_data): 
    title.append(google_data[count][0])
    documents.append(google_data[count][1])
    count+=1



conn.close()


# In[35]:


documents2 = documents
documents=[]


# In[36]:


for st in documents2:
    str2 = (st.encode('ascii', 'ignore')).decode("utf-8")
    str3 = str2.replace("\n", " ")
    documents.append(str3)


# In[37]:
'''

import nltk
import logging
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords

nltk.download('punkt')
nltk.download('stopwords')
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
from nltk.stem.wordnet import WordNetLemmatizer
nltk.download('wordnet')
texts_tokenized = [[word.lower() for word in word_tokenize(document)] for document in documents]
english_stopwords = stopwords.words('english')
english_stopwords.append('sakai')
english_stopwords.append('jiras')
english_stopwords.append('longsight')
english_stopwords.append('cira')

texts_filtered_stopwords = [[word for word in document if not word in english_stopwords] for document in texts_tokenized]
#print(texts_filtered_stopwords)
english_punctuations = [',','.',':',';','?','!','(',')','[',']','@','&','#','%','$','{','}','--','-','’','“'
,'”']
texts_filtered = [[word for word in document if not word in english_punctuations] for document in texts_filtered_stopwords]
st = WordNetLemmatizer()
texts_stemmed = [[st.lemmatize(word) for word in document] for document in texts_filtered]
all_stems = sum(texts_stemmed,[])
stems_once = set(stem for stem in set(all_stems) if all_stems.count(stem) == 1)
texts_stemmed = [[stem for stem in text if stem not in stems_once] for text in texts_stemmed]
texts_stemmed = [[st.lemmatize(word,pos='v') for word in document] for document in texts_stemmed]
'''

# Above is how the 'texts_stemmed' come from, below is the way I store it, please find a way store it into your database. Otherwise, the last step will cost you a lot of time.

# In[18]:


'''
import pickle
with open("/content/drive/My Drive/Colab Notebooks/tstem.txt", "wb") as fp:
  pickle.dump(texts_stemmed, fp)

fp.close()
'''
import pickle
with open("tstem.txt", "rb") as fp:   # Unpickling
  b = pickle.load(fp)

fp.close()
texts_stemmed = b


# In[19]:


contraction_mapping = {"ain't": "is not", "aren't": "are not","can't": "cannot", "'cause": "because", "could've": "could have", "couldn't": "could not",
                           "didn't": "did not",  "doesn't": "does not", "don't": "do not", "hadn't": "had not", "hasn't": "has not", "haven't": "have not",
                           "he'd": "he would","he'll": "he will", "he's": "he is", "how'd": "how did", "how'd'y": "how do you", "how'll": "how will", "how's": "how is",
                           "I'd": "I would", "I'd've": "I would have", "I'll": "I will", "I'll've": "I will have","I'm": "I am", "I've": "I have", "i'd": "i would",
                           "i'd've": "i would have", "i'll": "i will",  "i'll've": "i will have","i'm": "i am", "i've": "i have", "isn't": "is not", "it'd": "it would",
                           "it'd've": "it would have", "it'll": "it will", "it'll've": "it will have","it's": "it is", "let's": "let us", "ma'am": "madam",
                           "mayn't": "may not", "might've": "might have","mightn't": "might not","mightn't've": "might not have", "must've": "must have",
                           "mustn't": "must not", "mustn't've": "must not have", "needn't": "need not", "needn't've": "need not have","o'clock": "of the clock",
                           "oughtn't": "ought not", "oughtn't've": "ought not have", "shan't": "shall not", "sha'n't": "shall not", "shan't've": "shall not have",
                           "she'd": "she would", "she'd've": "she would have", "she'll": "she will", "she'll've": "she will have", "she's": "she is",
                           "should've": "should have", "shouldn't": "should not", "shouldn't've": "should not have", "so've": "so have","so's": "so as",
                           "this's": "this is","that'd": "that would", "that'd've": "that would have", "that's": "that is", "there'd": "there would",
                           "there'd've": "there would have", "there's": "there is", "here's": "here is","they'd": "they would", "they'd've": "they would have",
                           "they'll": "they will", "they'll've": "they will have", "they're": "they are", "they've": "they have", "to've": "to have",
                           "wasn't": "was not", "we'd": "we would", "we'd've": "we would have", "we'll": "we will", "we'll've": "we will have", "we're": "we are",
                           "we've": "we have", "weren't": "were not", "what'll": "what will", "what'll've": "what will have", "what're": "what are",
                           "what's": "what is", "what've": "what have", "when's": "when is", "when've": "when have", "where'd": "where did", "where's": "where is",
                           "where've": "where have", "who'll": "who will", "who'll've": "who will have", "who's": "who is", "who've": "who have",
                           "why's": "why is", "why've": "why have", "will've": "will have", "won't": "will not", "won't've": "will not have",
                           "would've": "would have", "wouldn't": "would not", "wouldn't've": "would not have", "y'all": "you all",
                           "y'all'd": "you all would","y'all'd've": "you all would have","y'all're": "you all are","y'all've": "you all have",
                           "you'd": "you would", "you'd've": "you would have", "you'll": "you will", "you'll've": "you will have",
                           "you're": "you are", "you've": "you have"}


# In[20]:


import nltk
nltk.download('stopwords')
from nltk.corpus import stopwords
from bs4 import BeautifulSoup
stop_words = set(stopwords.words('english')) 
import re


# In[21]:


def text_cleaner(text):
    newString = text.lower()
    newString = BeautifulSoup(newString, "lxml").text
    newString = re.sub(r'\([^)]*\)', '', newString)
    newString = re.sub('"','', newString)
    newString = ' '.join([contraction_mapping[t] if t in contraction_mapping else t for t in newString.split(" ")])    
    newString = re.sub(r"'s\b","",newString)
    newString = re.sub("[^a-zA-Z]", " ", newString) 
    tokens = [w for w in newString.split() if not w in stop_words]
    long_words=[]
    for i in tokens:
        if len(i)>=3:                  #removing short word
            long_words.append(i)   
    return (" ".join(long_words)).strip()


# In[75]:


from atlassian import Jira
url = "https://jira.sakaiproject.org" 
jira = Jira(url)
#This key should be entered by the user
Issue_key = 'SAK-40672'
#Issue = jira.jql(Issue_key)
try:
    issue = jira.issue(Issue_key)
except requests.ConnectionError as e:
    print("Please ensure that you enter the correct Issue key")

description = issue['key'] #issue id
description += ' '
description += issue['fields']['created'] # Issue created time
description += ' '
description += issue['fields']['summary'] # Issue's title
description += ' '
description += issue['fields']['creator']['displayName'] # Creator's name
description += ' '
description += issue['fields']['reporter']['displayName'] # Reporter's name
description += ' '

description += issue['fields']['description'] #Issue description


# In[76]:

import nltk
nltk.download('punkt')
from nltk.tokenize import word_tokenize
from nltk.stem.wordnet import WordNetLemmatizer
nltk.download('wordnet')

import warnings
warnings.filterwarnings(action='ignore', category=UserWarning, module='gensim')
from gensim.models import word2vec
import logging
 

import gensim
import numpy as np
from scipy.linalg import norm

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

model_file = 'GoogleNews-vectors-negative300.bin.gz'
model = gensim.models.KeyedVectors.load_word2vec_format(model_file, binary=True)


    


#Calculating Cos value
def vector_similarity(s1, s2, model2):
  if len(s1)==0:
    return 0
  elif len(s2)==0:
    return 0
  def sentence_vector(s):
    words = word_tokenize(s)
    v = np.zeros(300)
    for word in words:
      if word in model:
        v += model[word]
      elif word in model2:
        v += model2[word]
      else:
        v += np.zeros(300)
    v /= len(words)
    return v
  v1, v2 = sentence_vector(s1), sentence_vector(s2)
  return np.dot(v1,v2)/(norm(v1) * norm(v2))


# similarity result

# In[ ]:

        

def Similairties(description):
    simm=[]
    order=[]
    similar=0
    similar1=0
    cc=0
    cc1=0
    a = 1
    st = WordNetLemmatizer()
    description = text_cleaner(description)
    d_tok=word_tokenize(description)
    d_lem = [st.lemmatize(word) for word in d_tok]
    d_lem = [st.lemmatize(word,pos='v') for word in d_tok]
    dscr = " ".join(d_lem)    
    list1=[]
    for i in texts_stemmed:
        for x in i:
            list1.append(x)     
    sentences = list1  
    n_dim=64
    model2 = word2vec.Word2Vec([sentences], size=300, min_count=1,sg=1) 
    documents4=[]
    for i in texts_stemmed:
        documents4.append(" ".join(i))
    for i in range(0,len(documents)):
        similar=vector_similarity(dscr, documents4[i], model2)
        order.append(i)
        simm.append(similar)
        if similar>similar1:
            similar1=similar
            cc1=cc
        cc=cc+1
    zipped = zip(order,simm)
    sort_zipped = sorted(zipped,key=lambda x:(x[1],x[0]),reverse=True)
    '''
    for i in range(0,20):
        print(sort_zipped[i])
        print(title[sort_zipped[i][0]])
    '''
    
    return(sort_zipped)
        
'''
a = Similairties(description)


for i in range(0,20):
        print(a[i])
        print(title[a[i][0]])

# In[74]:


#print(links[1325])

'''