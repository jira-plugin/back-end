# -*- coding: utf-8 -*-
"""
Created on Sun Aug  9 23:09:40 2020

@author: Pavithran
"""


from __future__ import print_function
from atlassian import Confluence
import re
from bs4 import BeautifulSoup
from urllib.parse import urlparse
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from googleapiclient.errors import HttpError as GoogleHttpError

confluence = Confluence("https://confluence.sakaiproject.org")
b = confluence.get_all_spaces(start=0, limit=500, expand=None)
a = confluence.get_all_pages_from_space('BBB', start=0, limit=1000, status=None, expand=None, content_type='page')